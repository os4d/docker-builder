#!/usr/bin/env sh
TAG="$1"

contains() {
    string=$($1)
    substring=$2
    if test "${string#*$substring}" != "$string"
    then
        return 0    # $substring is in $string
    else
        echo "'$substring'" "not found in" "'$string'";
        return 1    # $substring is not in $string
    fi
}
echo "Running tests on ${CI_REGISTRY_IMAGE}:${TAG}"

contains "docker run --rm --tty -t ${CI_REGISTRY_IMAGE}:${TAG} rancher --version" version
contains "docker run --rm --tty -t ${CI_REGISTRY_IMAGE}:${TAG} lazo --version" version
contains "docker run --rm --tty -t ${CI_REGISTRY_IMAGE}:${TAG} docker --version" version
contains "docker run --rm --tty -t ${CI_REGISTRY_IMAGE}:${TAG} glab --version" version
contains "docker run --rm --tty -t ${CI_REGISTRY_IMAGE}:${TAG} sentry-cli --version" sentry-cli
