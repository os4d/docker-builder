#!/bin/sh

echo "
- Docker Builder '${VERSION}' build '${BUILD_DATE}'
"
echo "`pipx list --short | sort`
`git --version | awk '{print $1 " " $3}'`
`rancher --version | awk '{print $1 " " $3}'`
`sentry-cli --version | awk '{print $1 " " $2}'`
`glab --version | awk '{print $1 " " $3}'`" | sort

echo ""
