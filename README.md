# docker-builder

[![pipeline status](https://gitlab.com/os4d/docker-builder/badges/develop/pipeline.svg)](https://gitlab.com/os4d/docker-builder/-/commits/develop)
[![Latest Release](https://gitlab.com/os4d/docker-builder/-/badges/release.svg)](https://gitlab.com/os4d/docker-builder/-/releases)


Simple docker image built to be used as "working" image in CI pipelines 

## Provided tools:

- anybadge (https://pypi.org/project/anybadge/)
- bandit (https://bandit.readthedocs.io/en/latest/index.html)
- black (https://pypi.org/project/black/)
- docker (https://www.docker.com/)
- docker-squash (https://pypi.org/project/docker-squash/)
- flake8 (https://pypi.org/project/flake8/)
- genbadge (https://pypi.org/project/genbadge/)
- git (https://git-scm.com/)
- glab (https://gitlab.com/gitlab-org/)
- httpie (https://github.com/httpie/httpie)
- lazo (https://github.com/saxix/lazo)
- python-gitlab (https://pypi.org/project/python-gitlab/)
- wlc (https://docs.weblate.org/en/latest/wlc.html)
- mypy (https://mypy.readthedocs.io/en/latest/?badge=latest)