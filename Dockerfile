FROM docker:20
ARG BUILD_DATE
ARG VERSION

ENV PATH=/root/.local/bin:${PATH} \
    BUILD_DATE=${BUILD_DATE} \
    VERSION=${VERSION}

ENV RANCHER_VERSION=2.7.0

RUN /bin/echo "http://ftp.halifax.rwth-aachen.de/alpine/v3.16/main" >> /etc/apk/repositories && \
    echo "http://ftp.halifax.rwth-aachen.de/alpine/v3.16/community" >> /etc/apk/repositories && \
    apk update && \
    apk add docker docker-cli-compose



RUN apk add --no-cache --virtual .build-deps \
    grep \
    gcc \
    make \
    libffi-dev \
    linux-headers \
    musl-dev \
    python3-dev


RUN apk add --no-cache --virtual .run-deps \
    curl \
    git \
    jq \
    libffi \
    py3-pip \
    python3

#RUN curl -q \
#    'https://proget.makedeb.org/debian-feeds/prebuilt-mpr.pub' | gpg --dearmor | sudo tee /usr/share/keyrings/prebuilt-mpr-archive-keyring.gpg 1> /dev/null
#RUN echo "deb [signed-by=/usr/share/keyrings/prebuilt-mpr-archive-keyring.gpg] https://proget.makedeb.org prebuilt-mpr $(lsb_release -cs)" | sudo tee /etc/apt/sources.list.d/prebuilt-mpr.list
#RUN sudo apt update
#RUN apk add --no-cache glab
RUN export TRIVY_VERSION=$(wget -qO - "https://api.github.com/repos/aquasecurity/trivy/releases/latest" | grep '"tag_name":' | sed -E 's/.*"v([^"]+)".*/\1/')  \
    && echo $TRIVY_VERSION  \
    && wget --no-verbose https://github.com/aquasecurity/trivy/releases/download/v${TRIVY_VERSION}/trivy_${TRIVY_VERSION}_Linux-64bit.tar.gz -O - | tar -zxvf -

#RUN curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin v0.50.4
COPY templates /templates
RUN mkdir /data

RUN pip install pipx \
    && pipx install "lazo>=2.0.3" \
    && pipx install anybadge \
    && pipx install bandit \
    && pipx install codecov \
    && pipx install "flake8" \
    && pipx install "httpie" \
    && pipx inject flake8 flake8-html \
    && pipx install genbadge \
    && pipx install pdm \
    && pipx install isort \
    && pipx install black \
    && pipx install docker-squash \
    && pipx install tox \
    && pipx install wlc \
    && pipx install mypy


# GitLab client (glab)
RUN wget -c https://gitlab.com/gitlab-org/cli/-/releases/v1.40.0/downloads/glab_1.40.0_Linux_i386.tar.gz -O - | tar -xz bin/glab \
    && mv bin/glab /usr/local/bin/glab


# Sentry client
RUN curl -sL https://sentry.io/get-cli/ | SENTRY_CLI_VERSION="2.12.0" sh

# Sentry Rancher
RUN curl -L https://github.com/rancher/cli/releases/download/v${RANCHER_VERSION}/rancher-linux-amd64-v${RANCHER_VERSION}.tar.gz \
    -o rancher-cli.tar.gz \
    && tar -xf rancher-cli.tar.gz

# Sentry Lazo
RUN ln -s /root/.local/bin/lazo /usr/local/bin \
    && ln -s /rancher-v${RANCHER_VERSION}/rancher /usr/local/bin/


RUN set -ex \
    && apk del .build-deps \
    && rm -rf /var/cache/apk/* \
    && rm -fr /root/.cache/ \
    && rm -fr /usr/include/

COPY info.sh /usr/bin/info.sh