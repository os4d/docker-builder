VERSION?=dev
BUILD_DATE:=$(shell date +"%Y-%m-%d %H:%M")

.PHONY: help runlocal i18n

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z0-9_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

build:  ## build
	@echo "Building ${CI_REGISTRY_IMAGE}:${VERSION}"
	-@docker pull ${CI_REGISTRY_IMAGE}:latest

	@docker build \
		--cache-from ${CI_REGISTRY_IMAGE}:latest \
		--build-arg VERSION=${VERSION} \
		--build-arg BUILD_DATE="${BUILD_DATE}" \
		-t docker-builder.work \
		-f Dockerfile .
	@docker-squash -c docker-builder.work -t ${CI_REGISTRY_IMAGE}:${VERSION}
	echo Build ${CI_REGISTRY_IMAGE}:${VERSION}
	#@docker images | grep ${CI_REGISTRY_IMAGE}
	#@docker run --rm -it -t ${CI_REGISTRY_IMAGE}:${VERSION} info.sh

test:  # rur tests
	sh run_tests.sh ${VERSION}

info:  # retrieve image info
	 docker run --rm --tty -t ${CI_REGISTRY_IMAGE}:${VERSION} info.sh

release:
	docker tag ${CI_REGISTRY_IMAGE}:dev ${CI_REGISTRY_IMAGE}:${VERSION}
	docker push ${CI_REGISTRY_IMAGE}:dev

shell:  ## shell
	 docker run --rm -it \
 			-v ${PWD}/info.sh:/usr/bin/info.sh:rw \
 			-t ${CI_REGISTRY_IMAGE}:${VERSION} \
 			/bin/sh


register-gitlab-runner:
	gitlab-runner register \
			-n \
			--url https://gitlab.com/ \
			--registration-token ${GITLAB_RUNNER_TOKEN} \
			--executor docker \
			--docker-image registry.gitlab.com/os4d/docker-builder:19.03.12 \
			--tag-list "os4d/docker-builder" \
			--name gundam1 \
			--docker-privileged

	gitlab-runner register \
			-n \
			--url https://gitlab.com/ \
			--registration-token ${GITLAB_RUNNER_TOKEN} \
			--executor docker \
			--docker-image registry.gitlab.com/os4d/docker-builder:19.03.12 \
			--tag-list "os4d/docker-builder" \
			--name gundam2 \
			--docker-privileged